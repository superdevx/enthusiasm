# Enthusiasm

Based on https://github.com/arnau/Vigor

Needs [Homebrew](https://brew.sh/)

## Install

```sh
git clone https://gitlab.com/superdevx/enthusiasm ~/.vim
cd ~/.vim
make install
```

## Licence

This is free and unencumbered public domain software. For more information,
see http://unlicense.org/ or the accompanying UNLICENSE file.
